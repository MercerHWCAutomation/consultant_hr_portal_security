#Created by Triveni Gherde on 10/11/2017.
Feature: Consulant Portal Security Scenarios

  @Security @CP-LoginAuthorizationTesting@1
  Scenario: Verify that non-consultant user cannot access the consultant Portal
    Given   User opens the browser and launch the consultant Portal URl
    When    Enter non-consultant username and password and click Login on consultant Portal Login Page
    Then    User must not be able to Login into Consultant Portal


  @Security @CP-BypassingAuthenticationSchemaTesting @2
  Scenario: Verify that a user is not able to access the file/page of the CP-application before login
    Given   User opens the browser and launch the consultant Portal URl
    When    Try to access the Consultant page without Login
    Then    Page should not open and ppt should get redirected to CP-login page

  @Security @CP-ErrorCodeTesting @3
  Scenario: verify the error message after making invalid attempt on CP login page.
    Given   User opens the browser and launch the consultant Portal URl
    When    Enter invalid username or password and click Login on Consultant Portal Login Page
    Then    User must not be able to Login into Consultant Portal
    Then    Check the error messages on Login page

  @Security @CP-UploadOfUnexpectedFileTypes @4
  Scenario: verify that the application does not allow the upload of unexpected file types
    Given   User opens the browser and launch the consultant Portal URl
    When    Enter valid username and password and click Login on Consultant Portal Login Page
    And     Consultant Portal Dashboard should be displayed
    And     Go to Upload File page in Consultant and upload unexpected file
    Then    Application does not allow the upload of unexpected file types and should Throws error
    Then    User able to logout Successfully from Consultant Portal


  @Security @CP-HTTPStrictTransportSecurity @5
  Scenario: To verify (https to http)security of  application
    Given   User opens the browser and launch the consultant Portal URl
    When    Change https to http and try to open the URL
    Then    Consultant site must not open with http site

  @Smoke @CP-IFrame @6
  Scenario: To verfiy whether Consultant Portal opens in an iframe
    Given  Open the Iframe-HTML file in any browser
    When   Check consultant-site is vulnerable or not

  @Security @CP-HRAdminPortalLockOutTesting @7
  Scenario: Verify that user gets locked out and after making no of invalid attempts to consultant login
    Given   User opens the browser and launch the consultant Portal URl
    When    Enter invalid username or password and click Login on Consultant Portal Login Page
    And     Enter invalid username or password and click Login on Consultant Portal Login Page
    And     Enter invalid username or password and click Login on Consultant Portal Login Page
    And     Enter invalid username or password and click Login on Consultant Portal Login Page
    And     Enter invalid username or password and click Login on Consultant Portal Login Page
    And     Enter invalid username or password and click Login on Consultant Portal Login Page
    Then    Consultant User's Account should get locked


  @Smoke @CP-CredentialsEncrypted  @8
  Scenario: To verify credentials should be encrypted in Consultant application
    Given   User opens the browser and launch the consultant Portal URl
    When    Check Username and password field
    Then    Enter Credentials and check that the credentials are encrypted

###Run the below script when servers are down
  @Smoke @CP-SecurityWhenServerIsDown  @9
  Scenario: To verify that when servers are down,the program codes must not be displayed
    Given   Server are down and Try to open the consultant Portal URl
    When    Verify that the program codes must not be displayed


  @Smoke @CP-SessionTimeout @10
  Scenario: To verify session timeout in Consultant Portal application
    Given   User opens the browser and launch the consultant Portal URl
    When    Enter valid username and password and click Login on Consultant Portal Login Page
    And     Consultant Portal Dashboard should be displayed
    And     User must not use the CP-application for the specified time limit
    Then    The CP-Application has automatically logged you out



