/**
 * Created by Triveni Gherde on 10/11/2017.
 */


var data = require('../../TestResources/HRAdmin_Security_GlobalData');
var Objects = require(__dirname + '/../../repository/HRAdminSecurityPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');

var LoginPageHR,DashboardPageHR,UploadAFile,ResetPassword;

function initializePageObjects(browser, callback) {
    var BPPage = browser.page.HRAdminSecurityPages();
    LoginPageHR = BPPage.section.LoginPageHR;
    DashboardPageHR = BPPage.section.DashboardPageHR;
    UploadAFile = BPPage.section.UploadAFile;
    ResetPassword = BPPage.section.ResetPassword;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the browser and launch the HRAdmin Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL = data.HRAdminURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPageHR.waitForElementVisible('@inputUsernameHRAP', data.longWait);
            });
        }
    });
    this.When(/^Enter non-HR username and password and click Login on HRAdmin Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            LoginPageHR.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsername1)
                .setValue('@inputPasswordHRAP',data.HRAdminPortalPassword1)
                .click('@ContinueButtonLoginHRAP');
        }
    });

    this.When(/^User must not be able to Login$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageHR.waitForElementVisible('@Error_Incorrect_credentials', data.longWait)
            .waitForElementVisible('@inputUsernameHRAP',data.LoginWait);
        }
    });

    this.When(/^Enter invalid username or password and click Login on HRAdmin Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            LoginPageHR.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsername2)
                .setValue('@inputPasswordHRAP',data.InvalidPassword1)
                .click('@ContinueButtonLoginHRAP')
                .waitForElementVisible('@Error_Incorrect_credentials', data.longWait);
        }
    });

    this.When(/^Enter invalid username or password and click Login on HRAdmin Portal Login Page1$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            LoginPageHR.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsernamett)
                .setValue('@inputPasswordHRAP',data.InvalidPassword1)
                .click('@ContinueButtonLoginHRAP')
                .waitForElementVisible('@Error_Incorrect_credentials', data.longWait);
        }
    });
    this.Then(/^User's Account should get locked$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageHR.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsernamett)
                .setValue('@inputPasswordHRAP',data.InvalidPassword1)
                .click('@ContinueButtonLoginHRAP')
                .waitForElementVisible('@AccountLockError', data.longWait)
                .waitForElementVisible('@inputUsernameHRAP',data.LoginWait);
        }
    });
    this.When(/^Try to access the HR admin page without Login$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            URL = data.HRAdminURL1;
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);
        }
    });
    this.Then(/^Page should not open and ppt should get redirected to login page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            browser.waitForElementNotPresent('@MyDashboardHeader', data.longWait);
            LoginPageHR.waitForElementVisible('@inputUsernameHRAP', data.longWait);
        }
    });
//@4
    this.Then(/^Check the error messages$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageHR.waitForElementVisible('@Error_Incorrect_credentials', data.longWait)
                .waitForElementVisible('@inputUsernameHRAP',data.LoginWait)
                .waitForElementNotPresent('@Invalid_Error1',data.LoginWait)
                .waitForElementNotPresent('@Invalid_Error2',data.LoginWait)
                .waitForElementNotPresent('@Invalid_Error3',data.LoginWait);

        }
    });
//@5
    this.When(/^Enter valid username and password and click Login on HRAdmin Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            console.log(data.HRAdminPortalPassword45)
            console.log(data.HRAdminPortalUsername45)
            LoginPageHR.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .clearValue('@inputUsernameHRAP')
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsername45)
                .clearValue('@inputPasswordHRAP')
                .setValue('@inputPasswordHRAP',data.HRAdminPortalPassword45)
                .click('@ContinueButtonLoginHRAP');
        }
    });
    this.When(/^HRAdmin Portal Dashboard should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DashboardPageHR.waitForElementVisible('@MyDashboardHeader', data.longWait)
                .waitForElementVisible('@ViewAllEmployeeButton', data.longWait)
                .waitForElementVisible('@EmployeeSnapshotTile', data.longWait)
                .waitForElementVisible('@ActivityStreamTile', data.longWait)
                .waitForElementVisible('@MessageCenterTile', data.longWait)
                .waitForElementVisible('@PayrollTile', data.longWait)
                .waitForElementVisible('@ReportingTile', data.longWait)
                .waitForElementVisible('@DashboardMenuFlyoverTab', data.longWait)
                .waitForElementVisible('@EmployeesMenuFlyover', data.longWait)
                .waitForElementVisible('@BenefitsMenuFlyover', data.longWait)
                .waitForElementVisible('@ReportingMenuFlyover', data.longWait)
                .waitForElementVisible('@PayrollMenuFlyover', data.longWait)
                .waitForElementVisible('@ResourcesMenuFlyover', data.longWait)
                .waitForElementVisible('@TestingMenuFlyover', data.longWait);
        }
    });
    this.When(/^Go to Upload File page and upload unexpected file$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            UploadAFile.waitForElementPresent('@EmployeesMenu',data.LoginWait)
            .click('@EmployeesMenu')
            .waitForElementPresent('@UploadFileOption',data.LoginWait)
            .click('@UploadFileOption')
            .waitForElementPresent('@ChooseOption',data.LoginWait)
            .click('@ChooseOption')
            .waitForElementPresent('@Continue',data.LoginWait)
            .click('@Continue');
           //.waitForElementPresent('@ChooseFile',data.LoginWait)//.click('@ChooseFile');
        browser.pause(5000);
        //browser.setValue('input[type="file"]',require('path').resolve(data.EmployeeDataFile));
        browser.setValue('input[type="file"]',require('path').resolve('C:/Users/raghun_app/Desktop/HR Admin data for auto run/QAf/Doc8.docx'));
            browser.pause(5000);
        UploadAFile.waitForElementVisible('@UploadFilebutton',data.LoginWait);
            browser.pause(2000);
            UploadAFile.click('@UploadFilebutton');

        }
    });
    this.Then(/^Application does not allow the upload of unexpected file types and Throws error$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            UploadAFile.waitForElementPresent('@ErrorIcon',data.LoginWait)
            .waitForElementPresent('@ErrorMessage',data.LoginWait);
            browser.pause(10000);

        }
});
    this.Then(/^User able to logout Successfully from HR Admin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {

            var xpath45="//div[@class='user-pic small']";
            var xpath101="//a[contains(text(),'Log Out')]";
            browser.useXpath().moveToElement(xpath45,0,1).pause(3000);
            browser.useXpath().click(xpath101);
            browser.pause(5000);
        }
        LoginPageHR.waitForElementPresent('@inputUsernameHRAP',data.LoginWait);
    });

//@6
    this.When(/^Change https to http and try opening the URL$/, function () {

        var URL2;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL2 = data.HRAdminURL2;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL2);
                browser.timeoutsImplicitWait(40000);
            });
        }
    });
    this.Then(/^URL must not open with http site$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {

            LoginPageHR.waitForElementPresent('@CantReachError', data.LoginWait);
        }
    });
//@7
    this.When(/^Open the HTML file in any browser$/, function () {

        var URL4;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL4 = data.HRAdminHTMLFile;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL4);
                browser.pause(10000);
            });
        }
    });
    this.When(/^Check site is vulnerable or not$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            var vulnerableText="//p[contains(text(),'This website is vulnerable to Clickjacking')]";
            var HRSite="//p[contains(text(),'Register your account now')]";
            var Xpathtt="//label[@for='Username']";
            browser.useXpath().waitForElementNotPresent(vulnerableText, data.longWait);
            browser.pause(5000)
                .frame('ITest')
            browser.useXpath().waitForElementNotPresent(HRSite, data.longWait)
            browser.useXpath().waitForElementNotPresent(Xpathtt, data.longWait);

        }
    });


//@8
    this.When(/^Click on the username field,Browser must not prompt the attacker with recently used usernames$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            LoginPageHR.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .click('@inputPasswordHRAP')
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsername)
                .setValue('@inputPasswordHRAP',data.HRAdminPortalPassword)
                .click('@ContinueButtonLoginHRAP');
        }
    });

//@7
    this.Then(/^verify when user logs into HR Admin Portal username and password are in the encrypted formate$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            LoginPageHR.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsername)
                .setValue('@inputPasswordHRAP',data.HRAdminPortalPassword)
                .click('@ContinueButtonLoginHRAP');
        }
    });

//@9
    this.When(/^Try to reset the password for a valid username$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            ResetPassword.waitForElementPresent('@ForgotLink', data.longWait)
                .click('@ForgotLink')
                .waitForElementPresent('@WhatDidUForget', data.longWait)
                .click('@WhatDidUForget')
                .waitForElementPresent('@OptionPassword', data.longWait)
                .click('@OptionPassword')
                .waitForElementPresent('@ContinuE', data.longWait)
                .click('@ContinuE')
                .waitForElementPresent('@ProvideInfo', data.longWait)
                .setValue('@Firstname', data.HRFirstname)
                .waitForElementPresent('@ProvideInfo', data.longWait)
                .waitForElementPresent('@ProvideInfo', data.longWait)
                .waitForElementPresent('@ProvideInfo', data.longWait)


            var xpath21="//span[contains(text(),'"+data.HRAdminPortalUsername+"')]";
            browser.useXpath().waitForElementVisible(xpath21, data.longWait);
            SecuritySettingHR.waitForElementVisible('@SecuritySetting', data.longWait)
                .waitForElementPresent('@USERNAME', data.longWait)
                .waitForElementPresent('@Password', data.longWait)
                .waitForElementPresent('@SECURITYQUESTIONS', data.longWait)
                .waitForElementPresent('@SECURITYQUESTIONS1', data.longWait)
                .waitForElementPresent('@SECURITYQUESTIONS1value', data.longWait)
                .waitForElementPresent('@SECURITYQUESTIONS2', data.longWait)
                .waitForElementPresent('@SECURITYQUESTIONS2value', data.longWait)
                .waitForElementPresent('@SECURITYAns1', data.longWait)
                .waitForElementPresent('@SECURITYAns1value', data.longWait)
                .waitForElementPresent('@SECURITYAns2', data.longWait)
                .waitForElementPresent('@SECURITYAns2value', data.longWait);


        }
    });
//@10
    this.When(/^User must not use the application for the specified time limit$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            //browser.waitForElementPresent('@Timeout',data.sessionTimeout).acceptAlert();
            browser.pause(1400000);
        }
    });
    this.Then(/^The Application has automatically logged you out$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageHR.waitForElementPresent('@inputUsernameHRAP',data.LoginWait);

        }
    });


    this.When(/^Enter valid username and password and click Login on HRAdmin Portal Login Page new$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        ResetPassword.waitForElementVisible('@inputUsernameHRAP', data.longWait)
                .clearValue('@inputUsernameHRAP')
                .setValue('@inputUsernameHRAP','Stark2@house.com')
                .clearValue('@inputPasswordHRAP')
                .setValue('@inputPasswordHRAP','sneha@12345')
                .click('@ContinueButtonLoginHRAP');

    });


    //@8encrypted
    this.When(/^Check the Username and password field$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageHR.waitForElementVisible('@HRUsernameType', data.longWait)
                .waitForElementVisible('@HRPasswordTypeBefore', data.longWait);
            //   .waitForElementVisible('@HRUsernameValueInDOMBefore', data.longWait);
        }
    });
    this.Then(/^Enter the Credentials and check that the credentials are encrypted$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageHR.waitForElementVisible('@inputUsernameHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',"abcdad")
                .setValue('@inputPasswordHRAP',"abcds")
                .click('@ContinueButtonLoginHRAP')
                .waitForElementVisible('@HRUsernameValueInDOMAfter', data.longWait)
                .waitForElementVisible('@HRPasswordTypeAfter', data.longWait)
                //.waitForElementVisible('@HRPasswordClassAfter', data.longWait)
                .waitForElementNotPresent('@HRPasswordClassBefore', data.longWait)
                .waitForElementNotPresent('@HRUsernameValueInDOMBefore', data.longWait);


        }
    });

    //@serversDown
    this.When(/^Server are down and Try to open the HRAdmin Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL = data.HRAdminURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
            });
        }
    });
    this.Then(/^Verify that the program codes must not be displayed when servers are down$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageHR.waitForElementNotPresent('@inputUsernameHRAP', data.longWait)
                .waitForElementNotPresent('@inputPasswordHRAP', data.longWait)
                .waitForElementNotPresent('@ContinueButtonLoginHRAP', data.longWait);
            DashboardPageHR.waitForElementNotPresent('@MyDashboardHeader', data.longWait)
                .waitForElementNotPresent('@ViewAllEmployeeButton', data.longWait)
                .waitForElementNotPresent('@EmployeeSnapshotTile', data.longWait)
                .waitForElementNotPresent('@ActivityStreamTile', data.longWait)
                .waitForElementNotPresent('@MessageCenterTile', data.longWait)
                .waitForElementNotPresent('@PayrollTile', data.longWait)
                .waitForElementNotPresent('@ReportingTile', data.longWait)
                .waitForElementNotPresent('@DashboardMenuFlyoverTab', data.longWait)
                .waitForElementNotPresent('@EmployeesMenuFlyover', data.longWait)
                .waitForElementNotPresent('@BenefitsMenuFlyover', data.longWait)
                .waitForElementNotPresent('@ReportingMenuFlyover', data.longWait)
                .waitForElementNotPresent('@PayrollMenuFlyover', data.longWait)
                .waitForElementNotPresent('@ResourcesMenuFlyover', data.longWait)
                .waitForElementNotPresent('@TestingMenuFlyover', data.longWait);


        }
    });

}

