/**
 * Created by Triveni Gherde on 10/4/2017.
 */

module.exports = {
    sections: {
        LoginPageHR: {
            selector: 'body',
            elements: {
                //QAF
                inputUsernameHRAP: {selector: '#Username'},
                inputPasswordHRAP: {selector: '#Password'},
                ContinueButtonLoginHRAP: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},
                Error_Incorrect_credentials: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'Either username or password is incorrect.')]"},
                Invalid_Error1: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'User does not exist')]"},
                Invalid_Error2: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'Username is incorrect.')]"},
                Invalid_Error3: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'Password is incorrect.')]"},
                AccountLockError: {locateStrategy: 'xpath',selector: "//div[@id='dialog'][contains( .,'Your account is locked.')]"},
                CantReachError: {locateStrategy: 'xpath',selector: "///*[@id=\"main-message\"]/h1"},
                Timeout: {locateStrategy: 'xpath',selector: "//div[@class='timeout-session']"},
                EncrypinputUsername: {locateStrategy: 'xpath',selector: "//div[@id='sessionTimeout']"},
                EncrypinputPassword: {locateStrategy: 'xpath',selector: "//input[@type='password']"},

                HRUsernameType :{locateStrategy: 'xpath',selector: "//input[@type='text']"},
                HRUsernameValueInDOMBefore :{locateStrategy: 'xpath',selector: "//input[@class='ng-pristine ng-valid ng-empty ng-touched']"},
                HRUsernameValueInDOMAfter :{locateStrategy: 'xpath',selector: "//input[@class='ng-valid ng-not-empty ng-dirty ng-valid-parse input-validation-error ng-touched']"},
                HRPasswordTypeBefore :{locateStrategy: 'xpath',selector: "//input[@type='password']"},
                HRPasswordClassBefore :{locateStrategy: 'xpath',selector: "//input[@class='input-validation-error']"},
                HRPasswordTypeAfter :{locateStrategy: 'xpath',selector: "//input[@type='password']"},
                HRPasswordClassAfter :{locateStrategy: 'xpath',selector: "//input[@class='valid']"},

            }
        },

        DashboardPageHR: {
            selector: 'body',
            elements: {
                //QAF
                MyDashboardHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'MY DASHBOARD')]"},
                DayToDayTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Day to Day')]"},
                OpenEnrollmentTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Open Enrollment')]"},
                EmployeeSnapshotTab: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Employee Snapshot')]"},
                DocumentLibraryQuicklinks:{locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..//a[contains(text(),'Document Library')]"},
                EducationQuicklinks: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..//a[contains(text(),'Education')]"},
                ViewAllEmployeeButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'View All Employees')]"},
                AddNewEmployeeButton: {locateStrategy: 'xpath',selector: "//a[contains(.,' Add New Employee')]"},

                //new Added
                EmployeeSnapshotTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Employee Snapshot')]"},
                ActivityStreamTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Activity Stream')]"},
                MessageCenterTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Message Center')]"},
                PayrollTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Payroll')]"},
                ReportingTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Reporting')]"},
                DashboardMenuFlyoverTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Dashboard')]"},
                EmployeesMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(.,'Employees')]"},
                BenefitsMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Benefits')]"},
                ReportingMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Reporting')]"},
                PayrollMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Payroll')]"},
                ResourcesMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(.,'Resources ')]"},
                TestingMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Testing')]"},
            }

        },
        UploadAFile: {
            selector: 'body',
            elements: {
                //QAF
                EmployeesMenu: {locateStrategy: 'xpath',selector: "//a[@class='ng-scope'][contains(text(),'Employees ')]"},
                UploadFileOption: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Upload a File')][@ng-click='gotoFileUpload()']"},
                UploadFileHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Upload A File')]"},
                ChooseOption: {locateStrategy: 'xpath',selector: "//label[@for='template_alreadyhavefile']"},
                Continue: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Continue')]"},
                ChooseFile: {locateStrategy: 'xpath',selector: "//label[contains(.,'Choose File')]"},
                UploadFilebutton: {selector: "div.button-lead"},
                ErrorIcon: {locateStrategy: 'xpath',selector: "//div[@class='status-lead-icon-box']"},
                ErrorMessage:  {locateStrategy: 'xpath',selector: "//span[contains(text(),'Your file has been uploaded, but it contained errors.')][@class='ng-binding']"},




            }
        },

        ResetPassword: {
            selector: 'body',
            elements: {
                //QAF
                ForgotLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Forgot username or password?')]"},
                WhatDidUForget: {locateStrategy: 'xpath',selector: "//select[@name='whatDidYouForget']"},
                OptionPassword: {selector: "select[ng-model='forgotType'] option[label='Password']"},
                ContinuE: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},
                ProvideInfo: {locateStrategy: 'xpath',selector: "//h2[@ng-bind-html='provideUserInfo']"},
                inputPasswordHRAP: {selector: '#Password'},
                inputUsernameHRAP: {selector: '#Username'},
                ContinueButtonLoginHRAP: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},



            }
        },

    }
};
