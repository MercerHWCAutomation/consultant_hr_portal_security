/**
 * Created by Triveni Gherde on 10/4/2017.
 */

module.exports = {
    sections: {
        LoginPageCP: {
            selector: 'body',
            elements: {
                //QAF
                inputUsernameCP: {selector: '#Username'},
                inputPasswordCP: {selector: '#Password'},
                ContinueButtonLoginCP: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},

                Error_Incorrect_credentials: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'Either username or password is incorrect.')]"},
                Invalid_Error1: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'User does not exist')]"},
                Invalid_Error2: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'Username is incorrect.')]"},
                Invalid_Error3: {locateStrategy: 'xpath',selector: "//div[@class='alert-box radius evo-alert-box alert'][contains(.,'Password is incorrect.')]"},
                AccountLockError: {locateStrategy: 'xpath',selector: "//div[@id='dialog'][contains( .,'Your account is locked.')]"},
                CantReachError: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'This site can’t be reached')]"},
                Timeout: {locateStrategy: 'xpath',selector: "//div[@class='timeout-session']"},
                EncrypinputUsername: {locateStrategy: 'xpath',selector: "//div[@id='sessionTimeout']"},
                EncrypinputPassword: {locateStrategy: 'xpath',selector: "//input[@type='password']"},


                UsernameType :{locateStrategy: 'xpath',selector: "//input[@type='text']"},
                UsernameValueInDOMBefore :{locateStrategy: 'xpath',selector: "//input[@class='ng-pristine ng-valid ng-empty ng-touched']"},
                UsernameValueInDOMAfter :{locateStrategy: 'xpath',selector: "//input[@class='ng-valid ng-not-empty ng-dirty ng-valid-parse input-validation-error ng-touched']"},
                PasswordTypeBefore :{locateStrategy: 'xpath',selector: "//input[@type='password']"},
                PasswordClassBefore :{locateStrategy: 'xpath',selector: "//input[@class='input-validation-error']"},
                PasswordTypeAfter :{locateStrategy: 'xpath',selector: "//input[@type='password']"},
                PasswordClassAfter :{locateStrategy: 'xpath',selector: "//input[@class='valid']"},


            }
        },

        DashboardPageBP: {
            selector: 'body',
            elements: {
                //QAF
                DashboardTitleBP: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'My Dashboard')]"},
                ResourcesTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Resources')]"},
                EducationTab: {selector: "li[ng-class=\"{active: menuItem == 'education'}\"]"},
                ActiveClientsLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Active Clients')][@class='ng-binding']"},
                PendingClientStatusText: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Pending Client Status')]"},
                ActiveClientStatusText: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Summary of current active clients')]"},
                DashboardTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Dashboard')]"},
                PendingClientLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Pending Clients')][@class='ng-binding']"},
                ViewActiveClientsDetails: {locateStrategy: 'xpath',selector: "//a[@ui-sref='activeClients'][contains(.,'View Details')]"},
                ViewPendingClientsDetails: {selector: "a[ui-sref='pendingClients']"},
                DashboardLinkTab: {selector: "a[href='#/dashboard']"},
                ClientsLinkTab: {locateStrategy: 'xpath',selector: "//a[@href='#/dashboard']/../..//a[contains(text(),'Clients')]"},
                ReadyClientsNumber: {locateStrategy: 'xpath',selector: "//a[contains(.,'Clients')][@class='clients-amount ng-binding']/../../../../div[4]/div/div[2]/a/span"},


            }
        },

        UploadFile: {
            selector: 'body',
            elements: {
                //QAF
                EmployeeDataTitle:  {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Employee Data')]"},
                ChooseFile1: {locateStrategy: 'xpath',selector: "//label[contains(.,'Choose File')]"},
                UploadFilebutton: {selector: "div.button-lead"},
                ErrorIcon1: {locateStrategy: 'xpath',selector: "//div[@class='status-lead-icon-box']"},
                ErrorMessage1:  {locateStrategy: 'xpath',selector: "//span[contains(text(),'Your file has been uploaded, but it contained errors.')][@class='ng-binding']"},




            }
        },



    }
};
