/**
 * Created by Triveni Gherde on 10/11/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    TestingEnvironment: 'QAF',
    //TestingEnvironment: 'CITEST',
    //TestingEnvironment: 'PROD',
    //TestingEnvironment: 'CSO',

//QAF data
    ConsultantURL: 'https://consultant-qaf.mercermarketplace365plus.com/Account/Login',
    ConsultantURL1: 'https://consultant-qaf.mercermarketplace365plus.com/#/dashboard',
    ConsultantURL2: 'http://consultant-qaf.mercermarketplace365plus.com/Account/Login',
    //Non-consultant  user credentials
    //ConsultantlUsername1: 'Kapoorr11111@kitkat.com',
    //ConsultantPassword1: 'Test@123',
    ConsultantlUsername1: 'Stark1@house.com11',
    ConsultantPassword1: 'Welcome123@',

    //Invalid credentials
    ConsultantUsername2: 'qa11.triveni.gherde@gisqa.mercer.com',
    InvalidPassword1: 'Test@123',
    InvalidPassword2: 'Test@123',
    //Valid Credentials
    ConsultantlUsername3: 'qa11.triveni.gherde@gisqa.mercer.com',
    ConsultantPassword3: 'Mercer@12345',
    //Consultant Iframe
    ConsultantIframeFile: 'C:/Users/raghun_app/Desktop/Consultant portal data for auto run/ConsultantIframeQAF.html',
    //consultant-UploadFile
    UploadURL: 'https://consultant-qaf.mercermarketplace365plus.com/#/wizard/7661/2017/demographicUpload',


    //CITEST data
    // ConsultantURL: 'https://consultant-cit.mercermarketplace365plus.com/Account/Login',
    // ConsultantURL1: 'https://consultant-cit.mercermarketplace365plus.com/#/dashboard',
    // ConsultantURL2: 'http://consultant-cit.mercermarketplace365plus.com/Account/Login',
    //Non-consultant  user credentials
    //ConsultantlUsername1: 'qa124triveni.gherde@gisqa.mercer.com'',
    //ConsultantPassword1: 'Mercer09',
    //Invalid credentials
    //ConsultantUsername2: 'qacit.triveni.gherde@gisqa.mercer.com'',
    //InvalidPassword1: 'Test@123',
    //InvalidPassword2: 'Test@123',
    //Valid Credentials
    //ConsultantlUsername3: 'qacit.triveni.gherde@gisqa.mercer.com'',
    //ConsultantPassword3: 'Mercer07!,
    //Consultant Iframe
    //ConsultantIframeFile: 'C:/Users/raghun_app/Desktop/Consultant portal data for auto run/ConsultantIframeCIT.html',
    //consultant-UploadFile
    //UploadURL: 'https://consultant-qaf.mercermarketplace365plus.com/#/wizard/7661/2017/demographicUpload',

    //CSO data
    // ConsultantURL: 'https://consultant-cso.mercermarketplace365plus.com/Account/Login',
    // ConsultantURL1: 'https://consultant-cso.mercermarketplace365plus.com/#/dashboard',
    // ConsultantURL2: 'http://consultant-cso.mercermarketplace365plus.com/Account/Login',
    //Non-consultant  user credentials
    //ConsultantlUsername1: 'Neha.Kuna1@airfake.com',
    //ConsultantPassword1: 'Test@123',
    //Invalid credentials
    //ConsultantUsername2: 'Fname1.Lname1@kitkat.com',
    //InvalidPassword1: 'Test@1231',
    //InvalidPassword2: 'Test@1231',
    //Valid Credentials
    //ConsultantlUsername3: 'Fname1.Lname1@kitkat.com',
    //ConsultantPassword3: 'Welcome@123!!',
    //Consultant Iframe
    //ConsultantIframeFile: 'C:/Users/raghun_app/Desktop/Consultant portal data for auto run/ConsultantIframeCSO.html',
    //consultant-UploadFile
    //UploadURL: 'https://consultant-qaf.mercermarketplace365plus.com/#/wizard/7661/2017/demographicUpload',

    //Prod data
    // ConsultantURL: 'https://consultant.mercermarketplace365plus.com',
    // ConsultantURL1: 'https://consultant.mercermarketplace365plus.com/#/dashboard',
    // ConsultantURL2: 'http://consultant.mercermarketplace365plus.com',
    //Non-consultant  user credentials
    //ConsultantlUsername1: 'Neha.Kuna1@airfake.com',
    //ConsultantPassword1: 'Test@123',
    //Invalid credentials
    //ConsultantUsername2: 'Barbara.meldrum@analystmercer.com',
    //InvalidPassword1: 'Test@1231',
    //InvalidPassword2: 'Test@1231',
    //Valid Credentials
    //ConsultantlUsername3: 'Barbara.meldrum@analystmercer.com',
    //ConsultantPassword3: 'Mercer4!',
    //Consultant Iframe
    //ConsultantIframeFile: 'C:/Users/raghun_app/Desktop/Consultant portal data for auto run/ConsultantIframeProd.html',
    //consultant-UploadFile
    //UploadURL: 'https://consultant-qaf.mercermarketplace365plus.com/#/wizard/7661/2017/demographicUpload',



    shortWait: 30000,
    longWait: 30000,
    LoginWait: 30000,
    shortestWait: 3000,
    //sessionTimeout:1200000,


}
