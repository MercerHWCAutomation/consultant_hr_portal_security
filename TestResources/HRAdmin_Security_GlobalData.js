/**
 * Created by Triveni Gherde on 10/11/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    TestingEnvironment: 'QAF',
    //TestingEnvironment: 'CITEST',
    //TestingEnvironment: 'PROD',
    //TestingEnvironment: 'CSO',

//QAF data
    //HRAdminURl
    HRAdminURL: 'https://hr-qaf.mercermarketplace365plus.com/Account/Login',
    HRAdminURL1: 'https://hr-qaf.mercermarketplace365plus.com/HrPortal#/hr/4580/2017/dashboard',
    HRAdminURL2: 'http://hr-qaf.mercermarketplace365plus.com/Account/Login',
    //Non-HR Admin user credentials
    HRAdminPortalUsername1: 'qa11.sneha.mhetre@mercer.com',
    HRAdminPortalPassword1: 'Mercer1!',
    //Invalid credentials
    HRAdminPortalUsername2: 'Top.Frok2@airfake.com',
    InvalidPassword1: 'Test@12311',
    InvalidPassword2: 'Test@12311',
    HRAdminPortalUsernamett: 'Kapoorr11111@kitkat.com',
    //Valid Credentials
    HRAdminPortalUsername45: 'Stark2@house.com',
    HRAdminPortalPassword45: 'sneha@12345',
    //HRAdminURL: 'http://hr-qaf.mercermarketplace365plus.com',
   // HRAdminPortalUsername: 'QA@kitat.com',
  //  HRAdminPortalPassword: 'Test@123!',

    //HR Profile
    Fname:'Treat1',
    Lname:'Tikkuji',
    SSN: '***-**-4288',
    DOB: '07/23/1980',
    Statusvalue: 'Part-Time',

    //HR Profile Edit
    HREffectiveDate: '10/9/2017',
    HRFirstname: 'Treat1',
    HRLastname: 'Tikkuji',
    HRAddressLine1: 'AddressLine1_1',
    HRAddressLine2: 'AddressLine2_2',
    HRCity: 'city1',
    HRZip: '12341',
    HRCurrentSalary: '20,001.00',
    HRBenefitSalary: '10,001.00',
    //HR Company
    CompanyName: 'Preconfigured_2017',
    AlertBoxValue: 'If changes are needed to your Company information, please contact your Consultant.',
    ConsultantName: 'Triveni Gherde',
    ConsultantPhone: '(716) 392-9245',
    ConsultantEmail: 'qa11.triveni.gherde@gisqa.mercer.com',
    GrantAccessMsg: 'Checking this box will enable your broker to see and do everything you can see and do on this site including Add/Edit Employee information and elections. If you want your broker to have this level of access, check the box below.',

    //Upload Employee Data
    EmployeeDataFile:'C:/Users/raghun_app/Desktop/HR Admin data for auto run/QAf/Doc5.docx',

    //Reset Password
    Last_Name: '(716) 392-9245',
    Last_Four_of_SSN:'',
    DateOfBirth:'',

    sessionTimeout:1150000,

//HRAdmin Notepad
    HRAdminHTMLFile: 'C:/Users/raghun_app/Desktop/HR Admin data for auto run/QAf/HRNotepad.html',


    //CITest data
    //HRAdminURL: 'https://hr-cit.mercermarketplace365plus.com/Account/Login',
    //HRAdminURL1: 'https://hr-cit.mercermarketplace365plus.com/HrPortal#/hr/4580/2017/dashboard',
    //HRAdminURL2: 'http://hr-cit.mercermarketplace365plus.com/Account/Login',
    //Non-HR Admin user credentials
    //HRAdminPortalUsername1: 'qacit.triveni.gherde@gisqa.mercer.com',
    //HRAdminPortalPassword1: 'Mercer07!',
    //Invalid credentials
    //HRAdminPortalUsername2: 'qa124triveni.gherde@gisqa.mercer.com',
    //InvalidPassword1: 'Test@123',
    //InvalidPassword2: 'Test@123',
    //Valid Credentials
    //HRAdminPortalUsername45: 'qa124triveni.gherde@gisqa.mercer.com',
    //HRAdminPortalPassword45: 'Mercer09',
    //Upload Employee Data
    //EmployeeDataFile:'C:/Users/raghun_app/Desktop/HR Admin data for auto run/QAf/Doc5.docx',
    //HRAdmin Notepad
    //HRAdminHTMLFile: 'C:/Users/raghun_app/Desktop/HR Admin data for auto run/IFrameHRCIT.html',

    //CSO data
    //HRAdminURL: 'https://hr-cso.mercermarketplace365plus.com/Account/Login',
    //HRAdminURL1: 'https://hr-cso.mercermarketplace365plus.com/HrPortal#/hr/4580/2017/dashboard',
    //HRAdminURL2: 'http://hr-cso.mercermarketplace365plus.com/Account/Login',
    //Non-HR Admin user credentials
    //HRAdminPortalUsername1: 'Fname1.Lname1@kitkat.com',
    //HRAdminPortalPassword1: 'Welcome@123!!',
    //Invalid credentials
    //HRAdminPortalUsername2: 'Neha.Kuna1@airfake.com',
    //InvalidPassword1: 'Test@12311',
    //InvalidPassword2: 'Test@123',
    //Valid Credentials
    //HRAdminPortalUsername45: 'Neha.Kuna1@airfake.com',
    //HRAdminPortalPassword45: 'Test@123',
    //Upload Employee Data
    //EmployeeDataFile:'C:/Users/raghun_app/Desktop/HR Admin data for auto run/QAf/Doc5.docx',
    //HRAdmin Notepad
    //HRAdminHTMLFile: 'C:/Users/raghun_app/Desktop/HR Admin data for auto run/IFrameHRCSO.html',


    //Prod data
    //HRAdminURL: 'https://hr.mercermarketplace365plus.com/Account/Login',
    //HRAdminURL1: 'hhttps://hr.mercermarketplace365plus.com/HrPortal#/hr/4580/2017/dashboard',
    //HRAdminURL2: 'http://hr.mercermarketplace365plus.com/Account/Login',
    //Non-HR Admin user credentials
    //HRAdminPortalUsername1: 'FBarbara.meldrum@analystmercer.com',
    //HRAdminPortalPassword1: 'Mercer4!',
    //Invalid credentials
    //HRAdminPortalUsername2: 'noreply1001@work.com,
    //InvalidPassword1: 'Test@12311',
    //InvalidPassword2: 'Test@123',
    //Valid Credentials
    //HRAdminPortalUsername45: 'noreply1001@work.com',
    //HRAdminPortalPassword45: 'Welcome@123',
    //Upload Employee Data
    //EmployeeDataFile:'C:/Users/raghun_app/Desktop/HR Admin data for auto run/QAf/Doc5.docx',
    //HRAdmin Notepad
    //HRAdminHTMLFile: 'C:/Users/raghun_app/Desktop/HR Admin data for auto run/IFrameHRProd.html',

    //HRAdminURL: '',
    //HRAdminPortalUsername: '',
    //HRAdminPortalPassword: '',



    shortWait: 30000,
    longWait: 30000,
    LoginWait: 30000,
    shortestWait: 3000,
    //sessionTimeout:1200000,


}
